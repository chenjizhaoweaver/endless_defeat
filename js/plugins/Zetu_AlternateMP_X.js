                            #======================#
                            #  Z-Systems by: Zetu  #
#===========================#======================#===========================#
#                 *  *  *  Z02 AMPX Ace Version v1.05  *  *  *                 #
#=#==========================================================================#=#
  #  You may need slight scripting knowledge of working with arrays (but     #
  #  not a whole lot!).                                                      #
  #--------------------------------------------------------------------------#
  #   * * Anything in "< >" means replace with the described contents. * *   #
  #  REQUIRED PARAMETERS (Any resource without it will cause an error)       #
  #    :name, <name of resource>                                             #
  #    :abbv, <1 or 2 character abbv. of resource>                           #
  #    :color, <color 1>, <color 2>                                          #
  #        * These color attributes are indexes of the window skin           #
  #  OPTIONAL PARAMETERS                                                     #
  #  Resource Regen for each turn:                                           #
  #    :regen, <type>, <base>                                                #
  #        type can be :mmp, :atk, :def, :mat, :agi, :set                    #
  #        base is number multiplied by type (:set is considered 1)          #
  #  Resource Regen on Damage                                                #
  #    :regendmg, <type>, <base>                                             #
  #        type can be :damage, :mmp, :set, :percentdmg                      #
  #        base in number multipled by type (:set is considered 1)           #
  #        (:percentdmg is ratio of mmp and damage)                          #
  #    :regenelemental, <element_id>, <type>, <base>                         #
  #        same as :regendmg, except on elemental damage (of                 #
  #        element_ids) (may place more than 1 id)                           #
  #  Resource Regen on Inflicting Damage                                     #
  #    :regenoffdmg, <type>, <base>                                          #
  #        type can be :damage, :mmp, :set, :percentdmg                      #
  #        base in number multipled by type (:set is considered 1)           #
  #        (:percentdmg is ratio of mmp and damage)                          #
  #  Resource Change Out of Battle:                                          #
  #    :depleteoob     (Set to 0)                                            #
  #    :regenoob       (Set to Max)                                          #
  #  Max Value                                                               #
  #    :max, <value>                                                         #
  #  Resource Inversion                                                      #
  #    :invert   #=// NOT INCLUDED IN THIS VERSION                           #
  #        this allows the resource to Add instead of Subtract for its cost  #
  #        and disallows skill use if cost will exceed the max resource.     #
  #--------------------------------------------------------------------------#
  #  NOTE :: Case Sensitivity Applies to Resource Names in these tags        #
  #    * I apologize and will fix this next patch *                          #
  #  REGEXP!                                                                 #
  #  <Class/Actor Note>                                                      #
  #    <AMPX: X>  #=> X = Name of Resource                                   #
  #      Adds the Resource to Actor.  If no resources exisit, will give      #
  #      actor DEF_RES as only resource.                                     #
  #  <Skill Note>                                                            #
  #    <AMPX DAMAGE: X>  #=> X = Name of Resource                            #
  #      When skill does MP damage, it will damage all resources with this   #
  #      tag. Will damage DEF_RES if tag does not exist.  Also allowed in    #
  #      Items.  (This tag also is used for healing)                         #
  #    <AMPX COST: X *Y>  #=> X = Name of Resource; *Y = Amount (Optional)   #
  #      Add cost to skill.  If not used, will cost default amount to first  #
  #      resource defined.  If *Y is not defined, will default in the        #
  #      default skill cost.                                                 #
  #  <State Note>                                                            #
  #    <AMPX REGEN: X>  #=> X = Name of Resource                             #
  #      Effect: MP Regeneration Rate will affect defined resource.  If not  #
  #      defined, will result in DEF_RES                                     #
  #    <AMPX COST: X>  #=> X = Name of Resource                              #
  #      Effect: MP Cost Rate will affect defined resource.  If not          #
  #      defined, will result in DEF_RES                                     #
  #--------------------------------------------------------------------------#
  #  Getting Values of Resources (ADVANCED)                                  #
  #  <Battler>.resource(NAME) #=> <Game_Resource>                            #
  #  <Battler>.resources #=> <Game_Resource[]> Array                         #
  #  
  #==========================================================================#
module Z02
  
  RESOURCES = [[
      :regen, :mmp, 0.05,
      :name, "Mana",
      :abbv, "M",
      :color, 23, 22
    ],[
      :deplete,
      :regendmg, :percent, 120,
      :regen, :set, -5,
      :max, 100,
      :name, "Rage",
      :abbv, "R",
      :color, 10, 2
    ],[
      :replenish,
      :regen, :set, 20,
      :max, 100,
      :name, "Energy",
      :abbv, "E",
      :color, 6, 14
    ],[
      :replenish,
      :regen, :set, 30,
      :regendmg, :percent, -150,
      :max, 100,
      :name, "Focus",
      :abbv, "F",
      :color, 3, 11
    ],[
      :invert,
      :deplete,
      :regenelemental, 3, :percent, 80,
      :regenelemental, 4, :percent, -120,
      :regenelemental, 6, :percent, -120,
      :regen, :set, -30,
      :max, 150,
      :name, "Heat",
      :abbv, "H",
      :color, 2, 10
    ]
  ]
  
  DEF_RES = "Mana"
  
#========#======================#====#================================#========#
#--------#                      #----# DO NOT EDIT PAST THIS POINT!!! #--------#
#--------# End of Customization #----# Editing will cause death by    #--------#
#--------#                      #----# brain asplosions.              #--------#
#========#======================#====#================================#========#
  module REGEXP
    USED_RESOURCES  = /<AMPX[:]*\s*(\w+)>/i
    SCOST_WOVALUE   = /<AMPX\s*COST[:]*\s*(\w+)>/i
    SCOST_WVALUE    = /<AMPX\s*COST[:]*\s*(\w+)\s*(\d+)>/i
    DAMAGE_TARGET   = /<AMPX\s*DAMAGE[:]*\s*(\w+)>/i
    
    EFFECT_REGEN    = /<AMPX\s*REGEN[:]*\s*(\w+)>/i
    EFFECT_COSTRATE = /<AMPX\s*COST[:]*\s*(\w+)>/i
  end
end

($imported||={})[:z02]=true

class Data_Resource
  attr_reader :params, :regenelemental
  
  def initialize(index)
    array=Z02::RESOURCES[index]
    @params = {}
    @params[:regenelemental] = {}
    i=0
    while (i < array.size)
      case (symbol = array[i])
      when :regenelemental
        @params[symbol][array[i+=1]] = [array[i+=1], array[i+=1]]
      when :invert, :deplete, :replenish
        @params[symbol] = true
      when :abbv, :name, :max
        @params[symbol] = [array[i+=1]]
      when :regendmg, :regen, :color, :regenoffdmg
        @params[symbol] = [array[i+=1], array[i+=1]]
      else
        puts "Unusable symbol! ##{symbol}"
      end
      i+=1
    end
    $def_res_i = index if params[:name][0].upcase == Z02::DEF_RES
  end
  
end

$data_resources = (0...Z02::RESOURCES.size).map{|i| Data_Resource.new(i)}
$def_res_i = 0

class Game_Resources
  attr_reader :battler
  
  class Resource #==============================================================
    attr_reader :value, :index, :name
    
    def initialize(name, list)
      $data_resources.each_with_index do |data_resource,i|
        if data_resource.params[:name][0].upcase == name.upcase
          @name  = data_resource.params[:name][0]
          @index = i
          break
        end
      end
      @list = list
      @value = max
      replenish
    end
    
    def battler
      @list.battler
    end
    
    def value=(new_value)
      @value = [[new_value, 0].max, max].min
    end
    
    def max
      begin
        return params[:max][0]
      rescue
        return battler.mmp
      end
    end
    
    def replenish
      if params[:replenish]
        self.value = max
      elsif params[:deplete]
        self.value = 0
      end
    end
    
    def params
      return $data_resources[@index].params
    end
    
    def rate
      value.to_f / max
    end
    
    def include?(symbol)
      !params[symbol].nil?
    end
    
  end #==//==END class Resource=================================================
  
  def initialize(battler)
    @battler = battler
    @resources = []
    all_notes.scan(Z02::REGEXP::USED_RESOURCES) do |name|
      @resources << Resource.new(name[0], self)
    end
    @resources << Resource.new(Z02::DEF_RES, self) if @resources.empty?
  end
  
  def [](value)
    return @resources[value] if value.is_a? Integer
    return resource_of(value) if value.is_a? String
  end
  
  def size
    @resources.size
  end
  
  def resource_of(value)
    if value.is_a? String
      @resources.each.each do |resource|
        return resource if resource.name.upcase == value.upcase
      end
    elsif value.is_a? Integer
      @resources.each.each do |resource|
        return resource if resource.index == value
      end
    end
    return nil
  end
  
  def all_notes
    if @battler.actor?
      return @battler.class.note + "\n" + @battler.actor.note
    else
      return @battler.enemy.note
    end
  end
  
  def first_index
    @resources[0].index
  end
    
  def each
    @resources
  end
  
end

class Window_Base < Window
  #--------------------------------------------------------------------------
  # * New method: draw_actor_ampx
  #--------------------------------------------------------------------------
  def draw_actor_ampx(resource, x, y, width)
    color1, color2 = resource.params[:color]
    draw_gauge(x, y, width, resource.rate, 
        text_color(color1), text_color(color2))
    change_color(text_color(color2))
    draw_text(x, y, 30, line_height, resource.params[:abbv][0])
    draw_current_and_max_values(x, y, width, resource.value, resource.max,
      mp_color(resource.battler), normal_color)
  end
  #--------------------------------------------------------------------------
  # * Overwrite method: draw_actor_mp
  #--------------------------------------------------------------------------
  def draw_actor_mp(actor, x, y, width=124)
	  if actor.resources.nil?
	  	actor.setup(actor.id)
	  end
    width /= actor.resources.size
    offset = width
    if actor.resources.size != 1
      offset += 2
      width -= 1
    end
    for i in 0...actor.resources.size
      draw_actor_ampx(actor.resources[i], x+offset*i, y, width)
    end
  end
  
end

class Window_SkillList < Window_Selectable
  #--------------------------------------------------------------------------
  # * Alias method: draw_skill_cost
  #--------------------------------------------------------------------------
  alias :z02dsc :draw_skill_cost
  def draw_skill_cost(rect, skill)
    if @actor.skill_tp_cost(skill) > 0
      z02dsc(rect, skill)
    elsif @actor.skill_mp_cost(skill) > 0
      skill.skill_cost(@actor).each do |index, value|
        next if (resource = @actor.resources.resource_of(index)).nil?
        next if value <= 0
        change_color(text_color(resource.params[:color][0]),
          @actor.skill_cost_payable?(skill))
        draw_text(rect, value, 2)
        rect.width -= 32
      end
    end
  end
  
end

class Game_BattlerBase
  
  def skill_cost_payable?(skill)
    tp >= skill_tp_cost(skill) && ampx_skill_cost_payable?(skill)
  end
  
  def pay_skill_cost(skill)
    ampx_pay_skill_cost(skill)
    self.tp -= skill_tp_cost(skill)
  end
  
  def ampx_skill_cost_payable?(skill)
    skill.skill_cost(self).each do |index, value|
      return false if (resource = @resources.resource_of(index)).nil?
      return false if resource.value < value
    end
    return true
  end
  
  def ampx_pay_skill_cost(skill)
    skill.skill_cost(self).each do |index, value|
      next if (resource = @resources.resource_of(index)).nil?
      resource.value -= value
    end
  end
  
  def resource_obj(item)
    item.note.scan(Z02::REGEXP::DAMAGE_TARGET) do |name|
      unless (resource = @resources.resource_of(name[0])).nil?
        return resource
      end
      return nil
    end
    @resources[0]
  end
  
  def ampx_regenrate(xparam_id) #=//=
    rates = []
    rates.default = 0
    states.each do |state|
      state.features do |feature|
        next unless feature.code == FEATURE_XPARAM
        next unless feature.data_id == 8
        resource = resource_of(state.regen_resource)
        next if resource.nil?
        
      end
    end
  end
  
end

class Game_Battler < Game_BattlerBase
  
  alias :z02ote :on_turn_end
  def on_turn_end #=//=
    z02ote
    apply_regen
  end
  
  alias :z02ia :item_apply
  def item_apply(user, item) #=//=
    z02ia(user, item)
    if @result.hit?
      unless item.damage.none?
        ampx_ondamage(user, item.damage.element_id)
        resource = resource_obj(item)
        unless resource.nil?
          resource.value -= @result.mp_damage
          ####user.mp += @result.mp_drain#### NEED TO ADD DRAIN
        end
      end
    end
  end
  
  def ampx_ondamage(user, element_id)
    return if @result.hp_damage==0
    apply_damage_regen(@result.hp_damage, element_id)
    user.apply_offdamage_regen(@result.hp_damage)
  end
  
  def apply_regen
    return if dead?
    @resources.each.each do |resource|
      next unless resource.include? :regen
      type, base = resource.params[:regen]
      base = base.to_f/100
      case type
      when :mmp;   base *= resource.max
      when :atk;   base *= self.atk
      when :def;   base *= self.def
      when :spi;   base *= self.spi
      when :agi;   base *= self.agi
      end
      resource.value += base.to_i
    end
  end
  
  def apply_damage_regen(damage, element_id=0)
    return if dead?
    @resources.each.each do |resource|
      next unless resource.include? :regendmg
      type, base = resource.params[:regendmg]
      puts damage
      puts base
      case type
      when :mmp;     base *= max
      when :damage;  base *= damage
      when :percent; base *= damage.to_f/mhp
      end
      resource.value += base.to_i
      next if (array=resource.params[:regenelemental][element_id]).nil?
      type, base = array
      case type
      when :mmp;     base *= max
      when :damage;  base *= damage
      when :percent; base *= damage.to_f/mhp
      end
      resource.value += base.to_i
    end
  end
  
  def apply_offdamage_regen(damage)
    return if dead?
    @resources.each.each do |resource|
      next unless resource.include? :regenoffdmg
      type, base = resource.params[:regenoffdmg]
      case type
      when :mmp;     base *= max
      when :damage;  base *= damage
      when :percent; base *= damage.to_f/mhp
      end
      resource.value += base
    end
  end
  
end

class Game_Actor < Game_Battler
  attr_reader :resources
  
  alias :z02s :setup
  def setup(actor_id)
    z02s actor_id
    @resources = Game_Resources.new(self)
  end
  
end

class Game_Enemy
  attr_reader :resources
  
  alias :z02i :initialize
  def initialize(index, enemy_id)
    z02i index, enemy_id
    @resources = Game_Resources.new(self)
  end
  
end

class RPG::Skill < RPG::UsableItem
  
  def skill_cost(actor = nil)
    r = {}
    self.note.scan(Z02::REGEXP::SCOST_WOVALUE) do |name|
      $data_resources.each_with_index do |data_resource,i|
        next unless data_resource.params[:name][0].upcase == name[0].upcase
        r[i] = mp_cost
        break
      end
    end
    self.note.scan(Z02::REGEXP::SCOST_WVALUE) do |name, value|
      $data_resources.each_with_index do |data_resource,i|
        next unless data_resource.params[:name][0].upcase == name.upcase
        r[i] = value.to_i
        break
      end
    end
    if actor.nil?
      r[$def_res_i] = mp_cost if r.empty?
    else
      r[actor.resources.first_index] = mp_cost if r.empty?
    end
    return r
  end
  
end

module SceneManager
  #--------------------------------------------------------------------------
  # * Alias method: call
  #--------------------------------------------------------------------------
  class <<self; alias_method :z02call, :call; end
  def self.call(scene_class)
    self.z02call(scene_class)
    for actor in $game_party.members
	    next if actor.resources.nil?
      actor.resources.each do |resource|
        resource.replenish
      end
    end
  end
  
end